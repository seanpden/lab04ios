//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Sean Denney on 2/15/22.
//

import Foundation
import UIKit

class StudySetViewController: UIViewController {
    
    @IBOutlet var cardView: UIView!
    @IBOutlet var termLabel: UILabel!
    
    @IBOutlet var skipBut: UIButton!
    @IBOutlet var missedBut: UIButton!
    @IBOutlet var correctBut: UIButton!
    
    var isFlipped = false

    var flashcards: [Flashcard] = []
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        flashcards = Flashcard.getHardCodedCollection()
        updatePage()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(flipCard(_:)))
        cardView.addGestureRecognizer(gesture)
        
        
    }
    
    @IBAction func flipCard(_ sender:UITapGestureRecognizer) {
        termLabel.text = flashcards[index].definition
//        if (isFlipped) {
//            termLabel.text = flashcards[index].definition
//        }
//        else {
//            termLabel.text = flashcards[index].term
//        }
    }
    
    
    @IBAction func missedButton(_ sender: Any)
    {
        flashcards[index].missed = true
        index += 1
        restart()
        
        
        
    }
    
    @IBAction func skipButton(_ sender: Any)
    {
        index += 1
        restart()
    }
    
    @IBAction func correctButton(_ sender: Any)
    {
        flashcards.remove(at: index)
        restart()
    }
    
    func restart()
    {
        if index > flashcards.count - 1
        {
            index = 0
        }
        updatePage()
    }
    
    
    func updatePage()
    {
        if flashcards.count == 0 {
            termLabel.text = "fin"
            return
        }
        termLabel.text = flashcards[index].term
    }
    
}
