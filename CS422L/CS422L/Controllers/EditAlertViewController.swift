//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Sean Denney on 2/15/22.
//

import Foundation
import UIKit

class EditAlertViewController: UIViewController {
    
    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    
    var card: Flashcard = Flashcard()
    
    var parentVC: FlashCardSetDetailViewController?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        termEditText.text = card.term
        definitionEditText.text = card.definition
        
    }
    
    
    @IBAction func deleteButton() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func doneButton() {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
}
